# Kernel selection and removal tools for Void Linux

These scripts are simple ncurses interfaces for selecting and removing Linux Kernels for Void Linux.

The only dependency is dialog, and of course ncurses.

    $ xbps-install dialog

## vkselect

Manages symbolic links in `/boot`, no parameters are needed to run the script. At the end of the day it executes:

    $ unlink /boot/vmlinuz
    $ unlink /boot/initramfs
    $ ln -s vmlinuz-${version} /boot/vmlinuz
    $ ln -s initramfs-${version}.img /boot/initramfs

![vkselect](./vkselect.png "vkselect")

## vkremove

Allows to purge kernels, no parameters are needed to run the script. At the end of the day it just executes:

    $ vkpurge rm ${version} [${version}...]

![vkremove](./vkremove.png "vkremove")
